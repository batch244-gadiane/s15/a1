// this is a statement
console.log("Hello Batch244 !")

console.log("Hello World !") ;

console.
log
(
	"Hello Again !"
)

/*[Section] Variable - contains data.
//how do use variables, we use let and const for declaring variables. tells ur device that a variable is created and is ready to store data
Sytnax = let/const variableNAme
let pwde mg change ug value while const dli
*/

let myVariable;
console.log(myVariable); //result: undefined

// Initializing variables
//Syntax let/const variableName = value;

let productName = "desktop computer";
console.log(productName)

let productPrice = 18999;
console.log(productPrice);

const interest = 3.539;
console.log(interest);

//Reasigning values
//Syntax variableName = newValue;

productName = "Laptop";
console.log(productName);

let friend = "Kate";
console.log(friend);
friend = "Jane";
console.log(friend);


let friend1 = "Mark"
console.log(friend1)

/*
this will result error : Assisgnmet to a consatnt variable
interest = 4.489;
console.log(interest)
*/

/*
let/const local/global scope
*/

 let outerVariable = "Hello";
 {
 	let innerVariable = "Hello Again";

 }

 console.log(outerVariable);
 //console.log(innerVariable);//REesul;t undefined
 
 let productCode = "DC017";
 const productBrand = "Dell";
 console.log(productCode,productBrand);

 const greeting = "hi"
 console.log(greeting);

 //const and let cannot be use because it was alreaady reserved

 //[SECTION] DATA TYPES

	 /*
		String are series of charcters that create a word , a phrase , a sentence or anything related in creating text
		Strings in Java script can be written using either a single (') or (") qoute
	 */

 let country = 'Philippines';
 let province = "Bantangas";
 console.log(country,province);

 let fullAdress = province + "," + country;
 console.log(fullAdress);

 let greeting2 = "I live in the " + country + ".";
 console.log(greeting2);

 let message = 'John\'s employees went home early.';
 console.log(message);

 //Numbers
 //Integers/Whole Numbers
 let headCount = 26;
 console.log(headCount);

 //Decimals Numbers
 let grade = 98.7;
 console.log(grade);

 //exponential notation
 let planetDistance = 2e10;
 console.log(planetDistance);

 console.log("John's grade last quarter is " + grade);


 //Boolean - true/false

 let isMarried = false;
 let inGoodConduct = true;
 console.log("isMarried" + isMarried)
 console.log("inGoodConduct: " + inGoodConduct);

 //Arrays - use to store multiple values
 //Syntax ;  arrayName = [elementA, elementB, ....]

 let grades = [98, 92, 90, 94];
 console.log(grades);

 let details = ["John", "Smith" , 32, true];
 console.log(details);

/* Objects
syntax : let/const objectName ={}
		propertyA: Value
		propertyB:value
*/
 let person = {
 		fullName : "John Smith",
 		age : 32,
 		isMarried: true,
 		contact: ["0912345678", "09876554321"],
 		address : {
 				houseNumber : "345",
 				city : "Manila"

 		} 
}
 		console.log(person);